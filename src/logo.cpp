#include "logo.h"
//=======NUMBER======
//===constructor=====
Type::Type(int a){
	value.num=a;
	type=INT;
}
//=====assignment====
Type Type::operator=(int a){
	value.num=a;
	type=INT;
	return (*this);
}
//========WORD=======
//===constructor=====
Type::Type(const char* str){
	value.str=str;
	type=CHAR;
}
//===assignment======
Type Type::operator=(const char* str){
	value.str=str;
	type=CHAR;
	return (*this);
}
//========LIST=======
//====list creation===
Type  Type:: operator[](Type element){
	if(!element.seq){
		Type n;
		n.value.list=new vector<Type>();
		n.value.list->push_back(element);
		n.type=T_LIST;
		return n;
	}
	else{
		element.type=T_LIST;
		return (element);
	}
}
//======================
Type Type::operator=(Type t){
	if(type!=CHAR&&type!=INT){
		delete value.list;
	}
	value=t.value;
	type=t.type;
	return (*this);
}
//============ARRAY==========
Type::Type(std::initializer_list<Type> il){
	value.list = new vector<Type>(il);
	type=T_ARRAY;
}
//===========SENTANCE==============
Type Type::sentence(std::initializer_list<Type> il){
	value.list = new vector<Type>(il);
	for(int i=0;i<value.list->size();i++){
		if((*(value.list))[i].type!=CHAR){
			throw std::invalid_argument("received non WORD type of argument");
			exit(0);
		}
	}
	type=T_SENTENCE;
	return *this;
}
//===========COMMA===========
Type Type::operator,(Type t){
	if(!this->seq){
		Type n;
		n.value.list=new vector<Type>();
		n.value.list->push_back(*this);
		n.value.list->push_back(t);
		n.seq=true;
		return n;
	}
	else{
		Type n;
		this->value.list->push_back(t);
		n.value.list=this->value.list;
		n.seq=true;
		return n;
	}
}
//==========to string==============
std::string parsing(Type a){
	std::ostringstream stringStream;
	std::string copyOfStr;
	if(a.get_tag()==Type::INT){
		stringStream << a.get_value().num;
	}
	else if(a.get_tag()==Type::CHAR){
		   stringStream << a.get_value().str;
	}
	else if(a.get_value().list==NULL){
		   stringStream << "NULL";
	}
	else if(a.get_tag()==Type::T_LIST){
		vector<Type> l;
		l=*(a.get_value().list);
		stringStream<<"[";
		for(int i=0;i<l.size();i++)stringStream <<" "+parsing(l[i]);
		stringStream<<"]";
	}
	else if(a.get_tag()==Type::T_ARRAY){
		vector<Type> l;
		l=*(a.get_value().list);
		stringStream<<"{";
		for(int i=0;i<l.size();i++)stringStream <<" "+parsing(l[i]);
		stringStream<<"}";
	}
	else{
		vector<Type> l;
		l=*(a.get_value().list);
		stringStream<<"(";
		for(int i=0;i<l.size();i++)stringStream <<" "+parsing(l[i]);
		stringStream<<")";
	}
	copyOfStr = stringStream.str();
	return copyOfStr;
}
//==output operator overloading====
ostream& operator<<(ostream &out, const Type &a){
	out<<parsing(a);
	return out;
}
//====get value of variable====
Type::Value Type::get_value() {
	return value;
}
//====get type tag of variable====
Type::Tag Type::get_tag() {
	return type;
}
//===========get element==========
Type Type::get_element_arr(std::initializer_list<Type> il){
	vector<Type> l = vector<Type>(il);
	if(l.size()>2){
		throw std::invalid_argument("wrong number of argument");
		exit(0);
	}
	Type i=(l)[0];
	Type arr=(l)[1];
	if(arr.type!=T_ARRAY||i.type!=T_ARRAY){
		throw std::invalid_argument("wrong type of argument");
	}

	return get_element(i, arr);
}
Type Type::get_element_list(std::initializer_list<Type> il){
	vector<Type> l = vector<Type>(il);
	if(l.size()>2){
		throw std::invalid_argument("wrong number of argument");
		exit(0);
	}
	Type i=(l)[0];
	Type arr=(l)[1];
	if(arr.type!=T_LIST||i.type!=T_ARRAY){
		throw std::invalid_argument("wrong type of argument");
	}

	return get_element(i, arr);
}
Type Type::get_element(Type i, Type arr){
	int size_of_i;
	int size_of_arr;
	int j=0;
	Type ret;
	size_of_i=i.value.list->size();
	size_of_arr=arr.value.list->size();
	if(size_of_i>1){
		ret.type=T_ARRAY;
		ret.value.list=new vector<Type>();
	}
	for(int k=0;k<size_of_i;k++){
		if((*(i.value.list))[k].type!=INT){
			throw std::invalid_argument("wrong type of argument");
			exit(0);
		}
		j=(*(i.value.list))[k].value.num;
		if(j>size_of_arr||j<=0){
			throw std::invalid_argument("iteration went out of the bounds of the arr/list");
			exit(0);
		}
		if(size_of_i>1){
			ret.value.list->push_back((*(arr.value.list))[j-1]);
		}
		else{
			ret=(*(arr.value.list))[j-1];
		}
	}
	return ret;
}
//=========set element========
void Type::set_element(std::initializer_list<Type> il){
	vector<Type> *l = new vector<Type>(il);
	if(l->size()>3){
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	int size_of_i;
	int size_of_arr;
	int j=0;
	Type i=(*(l))[0];
	Type arr=(*(l))[1];
	Type new_el=(*(l))[2];
	if(arr.type!=T_ARRAY||i.type!=T_ARRAY){
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	size_of_arr=arr.value.list->size();
	if((*(i.value.list))[0].type!=INT){
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	j=(*(i.value.list))[0].value.num;
	if(j>size_of_arr||j<=0){
		throw std::invalid_argument("iteration went out of the bounds of the arr/list");
		exit(0);
	}
	(*(arr.value.list))[j-1]=new_el;
	l->clear();
}
//==========sum==========
Type Type:: sum(std::initializer_list<Type> il){
	vector<Type> *l = new vector<Type>(il);
	int size=l->size();
	int sum=0;
	for(int k=0;k<size;k++){
		if((*l)[k].type!=INT){
			throw std::invalid_argument("wrong type of argument");
			exit(0);
		}
		sum=sum+(*l)[k].value.num;
	}
	l->clear();
	type=INT;
	value.num=sum;
	return *this;
}
//===========difference========
Type Type :: difference(std::initializer_list<Type> il){
	vector<Type> *l = new vector<Type>(il);
	int size=l->size();
	if((*l)[0].type!=INT){
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	int minus=(*l)[0].value.num;
	for(int k=1;k<size;k++){
		if((*l)[k].type!=INT){
			throw std::invalid_argument("wrong type of argument");
			exit(0);
		}
		minus=minus-(*l)[k].value.num;
	}
	l->clear();
	type=INT;
	value.num=minus;
	return *this;
}
//==========minus==========
Type Type:: minus(Type arg){
	if(arg.type!=INT){
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	type=INT;
	value.num=-(arg.value.num);
	return *this;
}
//==========product==========
Type Type:: product(std::initializer_list<Type> il){
	vector<Type> *l = new vector<Type>(il);
	int size=l->size();
	int product=1;
	for(int k=0;k<size;k++){
		if((*l)[k].type!=INT){
			throw std::invalid_argument("wrong type of argument");
			exit(0);
		}
		product=product*(*l)[k].value.num;
	}
	l->clear();
	type=INT;
	value.num=product;
	return *this;
}
//==========quotient==========
Type Type:: quotient(std::initializer_list<Type> il){
	vector<Type> *l = new vector<Type>(il);
	int size=l->size();
	int quotient=(*l)[0].value.num;
	for(int k=1;k<size;k++){
		if((*l)[k].type!=INT){
			throw std::invalid_argument("wrong type of argument");
			exit(0);
		}
		quotient=quotient/(*l)[k].value.num;
	}
	l->clear();
	type=INT;
	value.num=quotient;
	return *this;
}
//==========modulo==========
Type Type:: modulo(std::initializer_list<Type> il){
	vector<Type> *l = new vector<Type>(il);
	int size=l->size();
	int modulo=(*l)[0].value.num;
	for(int k=1;k<size;k++){
		if((*l)[k].type!=INT){
			throw std::invalid_argument("wrong type of argument");
			exit(0);
		}
		modulo=modulo%(*l)[k].value.num;
	}
	l->clear();
	type=INT;
	value.num=modulo;
	return *this;
}
//=========equals===========
Type Type::operator==(Type t){
	Type ret;
	ret.type=INT;
	ret.value.num=1;
	if(type!=t.type)ret.value.num=0;
	else if(type==INT){
		if(value.num!=t.value.num)ret.value.num=0;
	}
	else if(type==CHAR){
		if(strcmp(value.str,t.value.str)==0)ret.value.num=0;
	}
	else{
		if(value.list!=t.value.list)ret.value.num=0;
	}
	return ret;
}
//=========equals===========
Type Type::operator!=(Type t){
	Type ret;
	ret.type=INT;
	ret.value.num=0;
	if(type!=t.type)ret.value.num=1;
	else if(type==INT){
		if(value.num!=t.value.num)ret.value.num=1;
	}
	else if(type==CHAR){
		if(strcmp(value.str,t.value.str)!=0)ret.value.num=1;
	}
	else{
		if(value.list!=t.value.list)ret.value.num=1;
	}
	return ret;
}
//=========smaller/equal===========
Type Type::operator<=(Type t){
	Type ret;
	ret.type=INT;
	ret.value.num=0;
	if(type!=t.type){
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	else if(type==INT){
		if(value.num<=t.value.num)ret.value.num=1;
	}
	else if(type==CHAR){
		if(strcmp(value.str,t.value.str)<=0)ret.value.num=1;
	}
	else{
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	return ret;
}
//=========bigger/equal===========
Type Type::operator>=(Type t){
	Type ret;
	ret.type=INT;
	ret.value.num=0;
	if(type!=t.type){
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	else if(type==INT){
		if(value.num>=t.value.num)ret.value.num=1;
	}
	else if(type==CHAR){
		if(strcmp(value.str,t.value.str)>=0)ret.value.num=1;
	}
	else{
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	return ret;
}
//=========bigger/equal===========
Type Type::operator>(Type t){
	Type ret;
	ret.type=INT;
	ret.value.num=0;
	if(type!=t.type){
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	else if(type==INT){
		if(value.num>t.value.num)ret.value.num=1;
	}
	else if(type==CHAR){
		if(strcmp(value.str,t.value.str)>0)ret.value.num=1;
	}
	else{
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	return ret;
}
//=========bigger/equal===========
Type Type::operator<(Type t){
	Type ret;
	ret.type=INT;
	ret.value.num=0;
	if(type!=t.type){
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	else if(type==INT){
		if(value.num<t.value.num)ret.value.num=1;
	}
	else if(type==CHAR){
		if(strcmp(value.str,t.value.str)<0)ret.value.num=1;
	}
	else{
		throw std::invalid_argument("wrong type of argument");
		exit(0);
	}
	return ret;
}
//=========not=========
Type Type :: operator!(){
	Type ret;
	ret.type=INT;
	ret.value.num=0;
	if(type==INT){
		if(value.num<=0)ret.value.num=1;
	}
	else if(type==CHAR){
		if(value.str==NULL)ret.value.num=1;
	}
	else if(type==N){
		ret.value.num=1;
	}
	else{
		if(value.list==NULL)ret.value.num=1;
	}
	return ret;
}
//==========and==========
Type Type:: _and(std::initializer_list<Type> il){
	vector<Type> *l = new vector<Type>(il);
	Type ret;
	Type tmp;
	ret.type=INT;
	ret.value.num=1;
	int size=l->size();
	for(int k=0;k<size;k++){
		tmp=(*l)[k];
		if(tmp.type==INT){
			if(tmp.value.num<=0){
				ret.value.num=0;
				return ret;
			}
		}
		else if(tmp.type==CHAR){
			if(tmp.value.str==NULL){
				ret.value.num=0;
				return ret;
			}
		}
		else if(tmp.type==N){
			ret.value.num=0;
			return ret;
		}
		else{
			if(tmp.value.list==NULL){
				ret.value.num=0;
				return ret;
			}
		}
	}
	l->clear();
	return ret;
}
//==========or==========
Type Type:: _or(std::initializer_list<Type> il){
	vector<Type> *l = new vector<Type>(il);
	Type ret;
	Type tmp;
	ret.type=INT;
	ret.value.num=0;
	int size=l->size();
	for(int k=0;k<size;k++){
		tmp=(*l)[k];
		if(tmp.type==INT){
			if(tmp.value.num>0){
				ret.value.num=1;
				return ret;
			}
		}
		else if(tmp.type==CHAR){
			if(tmp.value.str!=NULL){
				ret.value.num=1;
				return ret;
			}
		}
		else if(tmp.type!=N){
			ret.value.num=1;
			return ret;
		}
		else{
			if(tmp.value.list!=NULL){
				ret.value.num=1;
				return ret;
			}
		}
	}
	l->clear();
	return ret;
}
//-----conversion to boolean-----
Type::operator bool() const {
	if(type==INT && value.num != 0) return true;
	if(type==CHAR && value.str != NULL) return true;

	return false;
}
//------for making Type iterable------
Type::iterator Type::begin() {
	if(type != T_ARRAY && type != T_LIST && type != T_SENTENCE) {
		throw std::invalid_argument("variable is not iterable");
	}
	return value.list->begin();
}
Type::iterator Type::end() {
	if(type != T_ARRAY && type != T_LIST && type != T_SENTENCE) {
		throw std::invalid_argument("variable is not iterable");
	}
	return value.list->end();
}
Type::const_iterator Type::begin() const {
	if(type != T_ARRAY && type != T_LIST && type != T_SENTENCE) {
		throw std::invalid_argument("variable is not iterable");
	}
	return value.list->begin();
}
Type::const_iterator Type::end() const {
	if(type != T_ARRAY && type != T_LIST && type != T_SENTENCE) {
		throw std::invalid_argument("variable is not iterable");
	}
	return value.list->end();
}
Type::const_iterator Type::cbegin() const {
	if(type != T_ARRAY && type != T_LIST && type != T_SENTENCE) {
		throw std::invalid_argument("variable is not iterable");
	}
	return value.list->cbegin();
}
Type::const_iterator Type::cend() const {
	if(type != T_ARRAY && type != T_LIST && type != T_SENTENCE) {
		throw std::invalid_argument("variable is not iterable");
	}
	return value.list->cend();
}
//==============GUI API Function definitions===============
//-----function to initialize/destroy the gui----
int GuiAPI::initialize_gui(){
	int ret;
	ret = init_GUI();

	set_screen_color(255, 255, 255);
	set_pen_color(0, 0, 0);
	set_pen_thickness(0.1f);

	return ret;
}
void GuiAPI::destroy_gui(){
	destroy_GUI();
}
//-----movement command functions-----
int GuiAPI::forward(Type steps){
	if(steps.get_tag() != Type::INT){
		throw std::invalid_argument("wrong type of argument");
	}

	return turtle_mv_forward(steps.get_value().num);
}

int GuiAPI::backward(Type steps){
	if(steps.get_tag() != Type::INT){
		throw std::invalid_argument("wrong type of argument");
	}

	return turtle_mv_backward(steps.get_value().num);
}

void GuiAPI::left(Type angle){
	if(angle.get_tag() != Type::INT){
		throw std::invalid_argument("wrong type of argument");
	}

	turtle_rotate(-angle.get_value().num);
}

void GuiAPI::right(Type angle){
	if(angle.get_tag() != Type::INT){
		throw std::invalid_argument("wrong type of argument");
	}

	turtle_rotate(angle.get_value().num);
}

int GuiAPI::center(){
	return turtle_go_to_center();
}

int GuiAPI::setxy(Type x, Type y){
	if(x.get_tag() != Type::INT || y.get_tag() != Type::INT){
		throw std::invalid_argument("wrong type of argument");
	}

	return turtle_go_to_position(x.get_value().num, y.get_value().num);
}

void GuiAPI::circle(Type radius) {
	if(radius.get_tag() != Type::INT){
		throw std::invalid_argument("wrong type of argument");
	}
	return turtle_draw_circle(radius.get_value().num);
}

int GuiAPI::print(Type label) {
	if(label.get_tag() != Type::Type::CHAR){
		throw std::invalid_argument("wrong type of argument");
	}
	return turtle_draw_label(label.get_value().str);
}

//---pen and canvas command functions---
int GuiAPI::setpencolor(Type colors){
	vector<Type> l;
	l = *(colors.get_value().list);

	if(l.size() != 3){
		throw std::invalid_argument("incorrect count of arguments");
	}

	for(auto el: l) {
		if(el.get_tag() != Type::INT){
			throw std::invalid_argument("wrong type of argument");
		}
	}

	return set_pen_color(l[0].get_value().num, l[1].get_value().num, l[2].get_value().num);
}

int GuiAPI::setscreencolor(Type colors){
	vector<Type> l;
	l = *(colors.get_value().list);

	if(l.size() != 3){
		throw std::invalid_argument("incorrect count of arguments");
	}

	for(auto el: l) {
		if(el.get_tag() != Type::INT){
			throw std::invalid_argument("wrong type of argument");
		}
	}

	return set_screen_color(l[0].get_value().num, l[1].get_value().num, l[2].get_value().num);
}

void GuiAPI::setpensize(Type size){
	if(size.get_tag() != Type::INT){
		throw std::invalid_argument("wrong type of argument");
	}
	return set_pen_thickness(size.get_value().num);
}

void GuiAPI::pendown(){
	pen_down();
}

void GuiAPI::penup(){
	pen_up();
}
