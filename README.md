# cs352-proj

## Breakdown

### Types

- [x] `MAKE` keyword
	- [x] `WORD` type
	- [x] `NUMBER` type
	- [x] `BOOLEAN` type
	- [x] `LIST` type
	- [x] `ARRAY` type
	- [x] `SENTENCE` type

### Array Manipulation

- [x] `ITEM` keyword
- [x] `SETITEM` keyword

### Math Functions

- [x] `SUM` keyword
- [x] `DIFFERENCE` keyword
- [x] `MINUS` keyword
- [x] `PRODUCT` keyword
- [x] `QUOTIENT` keyword
- [x] `MODULO` keyword

### Flow Control functionality

- [x] Equality Operators `==, !=, >, >=, <, <=`
- [x] Logical Operators `(AND, OR, NOT)`
- [x] `IF ... DO ... ELSE ... END` statement
- [x] `REPCOUNT` keyword
- [x] `REPEAT N TIMES DO ... END` statement
- [x] `REPEAT WHILE ... DO ... END` statement
- [x] `FOREACH ... DO ... END`
	- [x] `ELEM` keyword

### Graphics Manipulation functionality

#### Canvas Movement

- [x] `FORWARD` keyword
- [x] `BACK` keyword
- [x] `LEFT` keyword
- [x] `RIGHT` keyword
- [x] `CENTER` keyword
- [x] `SETXY` keyword
- [x] `CIRCLE` keyword
- [x] `PRINT` keyword

#### Canvas/Pen Colour manipulation

- [x] `SETPENCOLOR` keyword
- [x] `SETSCREENCOLOR` keyword
- [x] `SETPENSIZE` keyword
- [x] `PENDOWN` keyword
- [x] `PENUP` keyword

#### Function Definition

- [x] `TO ... WITH ... FSTART ... FEND` statement
	- [x] `ARG` keyword
	- [x] `RETURN` keyword
	- [x] `CALL` keyword
