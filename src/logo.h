#include <iostream>
#include<string>
#include <vector>
#include <functional>
#include <sstream>
#include <initializer_list>
#include <cstdarg>
#include <stdarg.h>
#include <string.h>

#include "CS352_GUI_API/hy352_gui.h"
#ifdef APPLE
#include <allegro5/allegro.h>
#endif
//=======defines for new syntax=====
#define MAKE ; Type
#define NUMBER hidden =(false)?0
#define WORD hidden =(false)?0
#define BOOLEAN hidden =(false)?0
#define LIST hidden
#define ARRAY Type
#define SENTENCE(...) hidden.sentence({__VA_ARGS__})
#define TRUE (1)
#define FALSE (0)
#define AND(...) (hidden._and({__VA_ARGS__}))
#define OR(...) (hidden._or({__VA_ARGS__}))
#define MODULO(...) (hidden.modulo({__VA_ARGS__}))
#define QUOTIENT(...) (hidden.quotient({__VA_ARGS__}))
#define PRODUCT(...) (hidden.product({__VA_ARGS__}))
#define SUM(...) (hidden.sum({__VA_ARGS__}))
#define MINUS hidden.minus
#define ITEM(...) (hidden.get_element_arr({__VA_ARGS__}))
#define DIFFERENCE(...) (hidden.difference({__VA_ARGS__}))
#define SETITEM(...) ; hidden.set_element({__VA_ARGS__})
#define ASSIGN ;
#define NOT !
//========defines for {START,END}_PROGRAM===========
#define START_PROGRAM int main(){Type hidden;Type empty=Type(); \
	init_GUI();set_screen_color(255, 255, 255);set_pen_color(0, 0, 0);\
	set_pen_thickness(0.1f);
#define END_PROGRAM ;destroy_GUI();return 0;}
//=======defines for flow control statements========
//-----IF...DO...ELSE...END-----
#define IF ;{if(
#define ELSE }else{
//-----REPEAT...TIMES...DO...END-----
#define REPEAT ;{Type iter=Type(-1);while((
#define TIMES )>(iter=SUM (iter, (NUMBER: 1)))
/* This is so we can advance the iterator in the
 * while expression without affecting the result */
#define WHILE ((iter=SUM (iter, (NUMBER: 1)))||true))&&
#define REPCOUNT iter
//--------FOREACH...DO...END--------
#define FOREACH ;{for(auto elem:
#define ELEM elem
//--------common keywords DO, END--------
#define DO ){
#define END ;}}
//=======defines for function definition========
#define TO ;std::function<void(Type)>
#define WITH =[&](Type
#define FSTART ){Type hidden;
#define FEND };
#define CALL ;
#define ARG(...) hidden.get_element_list({{(__VA_ARGS__)}, args})
//=======defines for GUI API========
//-----movement command functions-----
#define FORWARD ;GuiAPI::forward
#define BACKWARD ;GuiAPI::backward
#define LEFT ;GuiAPI::left
#define RIGHT ;GuiAPI::right
#define CENTER ;GuiAPI::center()
#define SETXY ;GuiAPI::setxy
#define CIRCLE ;GuiAPI::circle
#define PRINT ;GuiAPI::print
//---pen and canvas command functions---
#define SETPENCOLOR ;GuiAPI::setpencolor
#define SETSCREENCOLOR ;GuiAPI::setscreencolor
#define SETPENSIZE ;GuiAPI::setpensize
#define PENDOWN ;GuiAPI::pendown()
#define PENUP ;GuiAPI::penup()
//==================================
class Type;
//========the type of each variable=======
class Type{
	using list_t = std::vector<Type>;

	public:
		using iterator = list_t::iterator;
		using const_iterator = list_t::const_iterator;
		//===the values of the variables====
		union Value{
			const char *str;
			int num;
			list_t *list=NULL;
		};
		//======the enumeration that shows==
		//===of what type of value has each variable===
		enum Tag{
			N,
			CHAR,
			INT,
			T_LIST,
			T_ARRAY,
			T_SENTENCE
		};
		//-----EMPTY-----
		Type(){type=N;};
		//----NUMBER----
		Type(int);
		Type operator=(int);
		//------WORD-----
		Type(const char*);
		Type operator=(const char*);
		//------LIST-------
		Type operator[](Type);
		Type operator=(Type);
		//-------ARRAY-------
		Type(std::initializer_list<Type> il);
		//------SENTENCE-----
		Type sentence(std::initializer_list<Type> il);
		//------COMMA--------
		Type operator,(Type arg);
		//-----output operator overloading-----
		friend ostream& operator<<(ostream &out, const Type &a);
		//-----get value of variable-----
		Value get_value();
		//----get type tag of variable----
		Tag get_tag();
		//-----get element-----
		Type get_element_arr(std::initializer_list<Type> il);
		Type get_element_list(std::initializer_list<Type> il);
		Type get_element(Type i, Type arr);
		//-----set element-----
		void set_element(std::initializer_list<Type> il);
		//--------sum--------
		Type sum(std::initializer_list<Type> il);
		//-------difference-------
        Type difference(std::initializer_list<Type> il);
		//-------minus-------
		Type minus(Type arg);
		//-------minus-------
		Type product(std::initializer_list<Type> il);
		//-------quotient----
		Type quotient(std::initializer_list<Type> il);
		//-------modulo-----
		Type modulo(std::initializer_list<Type> il);
		//-------equals------
		Type operator==(Type arg);
		//-------not equals------
		Type operator!=(Type arg);
		//-------smaller/equal------
		Type operator<=(Type arg);
		//-------bigger/equal------
		Type operator>=(Type arg);
		//---------bigger----------
		Type operator>(Type arg);
		//---------bigger----------
		Type operator<(Type arg);
		//---------not-------------
		Type operator!();
		//---------and-------------
		Type _and(std::initializer_list<Type> il);
		//---------and-------------
		Type _or(std::initializer_list<Type> il);
		//-----conversion to boolean-----
		explicit operator bool() const;
		//------for making Type iterable------
		iterator begin();
		iterator end();
		const_iterator begin() const;
		const_iterator end() const;
		const_iterator cbegin() const;
		const_iterator cend() const;

	private:
		//---local vars---
		Tag type;
		Value value;
		bool seq=false;
};
//========Class to interface with GUI API=======
class GuiAPI{
	public:
		//-----function to initialize/destroy the gui----
		static int initialize_gui();
		static void destroy_gui();
		//-----movement command functions-----
		static int forward(Type steps);
		static int backward(Type steps);
		static void left(Type angle);
		static void right(Type angle);
		static int center();
		static int setxy(Type x, Type y);
		static void circle(Type radius);
		static int print(Type label);
		//---pen and canvas command functions---
		static int setpencolor(Type colors);
		static int setscreencolor(Type colors);
		static void setpensize(Type size);
		static void pendown();
		static void penup();
};
